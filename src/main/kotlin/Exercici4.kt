
fun main() {
    val llistaElements = mutableListOf("element1", "element2", "element3")

    llistaElements.removeAll{ true }

    try {
        println(llistaElements.first())
    }catch (e: NoSuchElementException){
        println("No Such Element Exception")
    }

    val llistaElements2 = listOf("element1", "element2", "element3")
    llistaElements2 as MutableList

    try {
        llistaElements2.remove("element1")
    }catch (e: UnsupportedOperationException) {
        println("Unsupported Operation Exception")
    }
}


