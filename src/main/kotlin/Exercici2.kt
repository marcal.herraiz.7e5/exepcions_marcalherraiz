import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)
    val entrada = sc.next()
    println(aDoubleoU(entrada))
}

fun aDoubleoU(dividend: String): Double {
    val x: Double = try {
        dividend.toDouble()
    } catch (e: NumberFormatException) {
        1.0
    }
    return x
}
