import java.io.File
import java.io.IOException
import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)
    val fitxerLlegible = File("LoremIpsum.txt")
    llegirFitxer(fitxerLlegible)

    println("\n")

    val fitxerIllegible = File("Exercici1.kt")
    llegirFitxer(fitxerIllegible)

}

fun llegirFitxer(fitxer: File) {
    try {
        val x = fitxer.readText()
        println(x)
    }catch (e: IOException){
        println("$fitxer\n" +
                "(El sistema no pot trobar l'arxiu especificat")
    }
}

