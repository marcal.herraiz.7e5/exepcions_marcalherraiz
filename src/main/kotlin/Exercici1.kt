import java.util.Scanner

fun main() {
    val sc = Scanner(System.`in`)
    val dividend = sc.nextInt()
    val divisor = sc.nextInt()
    println(divideixoCero(dividend, divisor))
}

fun divideixoCero(dividend: Int, divisor: Int): Int {
    var resultat = 0
    try {
        resultat = dividend / divisor
    } catch (_: ArithmeticException) {
    }
    return resultat
}
